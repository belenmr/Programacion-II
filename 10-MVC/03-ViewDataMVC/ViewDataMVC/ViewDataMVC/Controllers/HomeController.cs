﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewDataMVC.Models;

namespace ViewDataMVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult ListarEscritores()
        {
            List<Escritor> escritores = new List<Escritor>()
            {
                new Escritor() {Nombre="Jorge Luis Borges"},
                new Escritor() {Nombre="Julio Cortazar"},
                new Escritor() {Nombre="Ernesto Sabato"},
                new Escritor() {Nombre="Jose Hernandez"}
            };

            ViewData["escritores"] = escritores;

            return View();
        }
    }
}