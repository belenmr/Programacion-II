﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisorVotosMVC.Models
{
    public class Votos
    {
        public Votos()
        {
            this.CandidatoA = 30;
            this.CandidatoB = 20;
            this.CandidatoC = 50;
        }

        public int CandidatoA { get; set; }
        public int CandidatoB { get; set; }
        public int CandidatoC { get; set; }
    }
}