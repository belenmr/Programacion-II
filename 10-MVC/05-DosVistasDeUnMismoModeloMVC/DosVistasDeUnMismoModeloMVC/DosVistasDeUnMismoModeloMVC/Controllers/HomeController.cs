﻿using DosVistasDeUnMismoModeloMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DosVistasDeUnMismoModeloMVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Listar()
        {
            List<Usuario> usuarios = new List<Usuario>();
            usuarios.Add(new Usuario() { Id = 10, Nombre = "César Milstein" });
            usuarios.Add(new Usuario() { Id = 11, Nombre = "Mister X" });
            usuarios.Add(new Usuario() { Id = 12, Nombre = "Cafrune" });

            return View(usuarios);
        }


        public ActionResult ListarDistinto()
        {
            List<Usuario> usuarios = new List<Usuario>();
            usuarios.Add(new Usuario() { Id = 10, Nombre = "César Milstein" });
            usuarios.Add(new Usuario() { Id = 11, Nombre = "Mister X" });
            usuarios.Add(new Usuario() { Id = 12, Nombre = "Cafrune" });

            return View(usuarios);
        }
    }
}