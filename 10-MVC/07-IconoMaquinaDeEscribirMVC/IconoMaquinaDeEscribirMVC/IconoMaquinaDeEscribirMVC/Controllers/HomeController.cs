﻿using IconoMaquinaDeEscribirMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IconoMaquinaDeEscribirMVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            Escritores escritores = new Escritores();
            return View(escritores);
        }

    }

}