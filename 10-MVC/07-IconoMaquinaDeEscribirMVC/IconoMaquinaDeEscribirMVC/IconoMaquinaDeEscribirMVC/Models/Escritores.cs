﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IconoMaquinaDeEscribirMVC.Models
{
    public class Escritores : List<Escritor>
    {
        public Escritores()
        {
            this.Add(new Escritor() { Nombre = "Italo Calvino", EscribeAMaquina = true });
            this.Add(new Escritor() { Nombre = "Jorge Luis Borges", EscribeAMaquina = false });
            this.Add(new Escritor() { Nombre = "Julio Cortázar", EscribeAMaquina = false });
            this.Add(new Escritor() { Nombre = "Ernesto Sabato", EscribeAMaquina = true });
            this.Add(new Escritor() { Nombre = "Edgar Allan Poe", EscribeAMaquina = true });
        }
    }
}