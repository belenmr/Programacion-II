﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IconoMaquinaDeEscribirMVC.Models
{
    public class Escritor
    {
        public string Nombre { get; set; }
        public bool EscribeAMaquina { get; set; }
    }
}