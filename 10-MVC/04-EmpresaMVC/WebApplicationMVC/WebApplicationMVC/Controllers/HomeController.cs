﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplicationMVC.Models;

namespace WebApplicationMVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            Empresa empresa = new Empresa();
            empresa.Nombre = "Abrigos que frío";
            empresa.FechaApertura = new DateTime(2000, 1, 1);
            empresa.Clientes.Add( new Cliente() { Id = 1, Nombre = "Sadosky", TieneDeuda = false});
            empresa.Clientes.Add(new Cliente() { Id = 2, Nombre = "Balseiro", TieneDeuda = false });
            empresa.Clientes.Add(new Cliente() { Id = 3, Nombre = "Leloir", TieneDeuda = true });
            empresa.Clientes.Add(new Cliente() { Id = 4, Nombre = "Favaloro", TieneDeuda = false });

            return View(empresa);
        }
    }
}