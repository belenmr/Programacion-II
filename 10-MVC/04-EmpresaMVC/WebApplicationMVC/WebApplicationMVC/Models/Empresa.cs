﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationMVC.Models
{
    public class Empresa
    {
        public Empresa()
        {
            this.Clientes = new List<Cliente>();
        }
        
        public string Nombre { get; set; }

        public DateTime FechaApertura { get; set; }

        public List<Cliente> Clientes { get; set; }
    }
}