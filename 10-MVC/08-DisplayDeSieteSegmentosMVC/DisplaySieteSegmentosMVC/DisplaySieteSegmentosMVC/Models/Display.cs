﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DisplaySieteSegmentosMVC.Models
{
    public class Display
    {
        public Display(int numero)
        {
            this.numeroRepresentado = numero;

            switch (numero)
            {
                case 0:
                    this.segmentoA = true;
                    this.segmentoB = true;
                    this.segmentoC = true;
                    this.segmentoD = true;
                    this.segmentoE = true;
                    this.segmentoF = true;
                    break;

                case 1:
                    this.segmentoB = true;
                    this.segmentoC = true;
                    break;

                case 2:
                    this.segmentoA = true;
                    this.segmentoB = true;
                    this.segmentoG = true;
                    this.segmentoE = true;
                    this.segmentoD = true;               
                    break;

                case 3:
                    this.segmentoA = true;
                    this.segmentoB = true;
                    this.segmentoG = true;
                    this.segmentoC = true;
                    this.segmentoD = true;                    
                    break;

                case 4:
                    this.segmentoF = true;
                    this.segmentoG = true;
                    this.segmentoB = true;
                    this.segmentoC = true;                    
                    break;

                case 5:
                    this.segmentoA = true;
                    this.segmentoF = true;
                    this.segmentoG = true;
                    this.segmentoC = true;
                    this.segmentoD = true;
                    break;

                case 6:
                    this.segmentoA = true;
                    this.segmentoF = true;
                    this.segmentoG = true;
                    this.segmentoC = true;
                    this.segmentoD = true;
                    this.segmentoE = true;
                    break;

                case 7:
                    this.segmentoA = true;
                    this.segmentoB = true;
                    this.segmentoC = true;
                    break;

                case 8:
                    this.segmentoA = true;
                    this.segmentoB = true;
                    this.segmentoC = true;
                    this.segmentoD = true;
                    this.segmentoE = true;
                    this.segmentoF = true;
                    this.segmentoG = true;
                    break;

                case 9:
                    this.segmentoC = true;
                    this.segmentoF = true;
                    this.segmentoG = true;
                    this.segmentoA = true;
                    this.segmentoB = true;
                    break;
            }
        }
        
        public bool segmentoA { get; set; }
        public bool segmentoB { get; set; }
        public bool segmentoC { get; set; }
        public bool segmentoD { get; set; }
        public bool segmentoE { get; set; }
        public bool segmentoF { get; set; }
        public bool segmentoG { get; set; }

        public int numeroRepresentado { get; set; }
    }
}