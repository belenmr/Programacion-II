﻿using DisplaySieteSegmentosMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DisplaySieteSegmentosMVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            int numRandom = new Random().Next(0, 9);
            Display model = new Display(numRandom);
            return View(model);
        }
    }
}