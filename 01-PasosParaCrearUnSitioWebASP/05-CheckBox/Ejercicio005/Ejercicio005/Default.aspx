﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Ejercicio005.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Ingrese primer valor: "></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:Label ID="Label2" runat="server" Text="Ingrese segundo valor: "></asp:Label>
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:CheckBox ID="CheckBox1" runat="server" Text="Sumar" />
            <br />
            <asp:CheckBox ID="CheckBox2" runat="server" Text="Restar" />
        </div>
        <p>
            <asp:Button ID="Button1" runat="server" Text="Operar" OnClick="Button1_Click" />
        </p>
        <asp:Label ID="Label3" runat="server"></asp:Label>
    </form>
</body>
</html>
