﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Ejercicio004.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Ingrese primer valor: "></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        </div>
        <asp:Label ID="Label2" runat="server" Text="Ingrese segundo valor: "></asp:Label>
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <div>
            <asp:RadioButton ID="RadioButton1" runat="server" Text="sumar" GroupName="operacion" />
            <br />
            <asp:RadioButton ID="RadioButton2" runat="server" Text="restar" GroupName="operacion" />
        </div>
        <p>
            <asp:Button ID="Button1" runat="server" Text="Resultado" OnClick="Button1_Click" />
        </p>
        <asp:Label ID="Label3" runat="server"></asp:Label>
    </form>
</body>
</html>
