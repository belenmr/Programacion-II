﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ejercicio02
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonEvaluar_Click(object sender, EventArgs e)
        {
            int respuestaCorrecta = 0;

            if (RadioButton1B.Checked)
            {
                respuestaCorrecta++;
            }

            if (RadioButton2C.Checked)
            {
                respuestaCorrecta++;
            }

            if (RadioButton3A.Checked)
            {
                respuestaCorrecta++;
            }

            if (RadioButton4A.Checked)
            {
                respuestaCorrecta++;
            }

            LabelResultado.Text = "Cantidad de respuestas correctas: " + respuestaCorrecta;
        }
    }
}