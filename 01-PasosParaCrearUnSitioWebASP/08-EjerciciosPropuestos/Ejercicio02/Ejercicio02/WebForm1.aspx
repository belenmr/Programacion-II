﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Ejercicio02.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Pregunta 1."></asp:Label>
        <p>
            <asp:RadioButton ID="RadioButton1A" runat="server" Text="Respuesta A" />
        </p>
        <asp:RadioButton ID="RadioButton1B" runat="server" Text="Respuesta B" />
        <p>
            <asp:RadioButton ID="RadioButton1C" runat="server" Text="Respuesta C" />
        </p>
        </div>

        <div>
            <asp:Label ID="Label2" runat="server" Text="Pregunta 2."></asp:Label>
        <p>
            <asp:RadioButton ID="RadioButton2A" runat="server" Text="Respuesta A" />
        </p>
        <asp:RadioButton ID="RadioButton2B" runat="server" Text="Respuesta B" />
        <p>
            <asp:RadioButton ID="RadioButton2C" runat="server" Text="Respuesta C" />
        </p>
        </div>

        <div>
            <asp:Label ID="Label3" runat="server" Text="Pregunta 3."></asp:Label>
        <p>
            <asp:RadioButton ID="RadioButton3A" runat="server" Text="Respuesta A" />
        </p>
        <asp:RadioButton ID="RadioButton3B" runat="server" Text="Respuesta B" />
        <p>
            <asp:RadioButton ID="RadioButton3C" runat="server" Text="Respuesta C" />
        </p>
        </div>

        <div>
            <asp:Label ID="Label4" runat="server" Text="Pregunta 4."></asp:Label>
        <p>
            <asp:RadioButton ID="RadioButton4A" runat="server" Text="Respuesta A" />
        </p>
        <asp:RadioButton ID="RadioButton4B" runat="server" Text="Respuesta B" />
        <p>
            <asp:RadioButton ID="RadioButton4C" runat="server" Text="Respuesta C" />
        </p>
        </div>
        <asp:Button ID="ButtonEvaluar" runat="server" OnClick="ButtonEvaluar_Click" Text="Evaluar" />
        <p>
            <asp:Label ID="LabelResultado" runat="server"></asp:Label>
        </p>
    </form>
</body>
</html>
