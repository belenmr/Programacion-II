﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ejercicio01
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonAceptar_Click(object sender, EventArgs e)
        {
            string nombre = TextBoxNombre.Text;
            string apellido = TextBoxApellido.Text;

            if (nombre.Length == 0 || apellido.Length ==0)
            {
                LabelResultado.Text = "Nombre y/o Apellido no ingresados";
            }
            else
            {
                LabelResultado.Text = "Datos ingresados correctamente";
            }
        }
    }
}