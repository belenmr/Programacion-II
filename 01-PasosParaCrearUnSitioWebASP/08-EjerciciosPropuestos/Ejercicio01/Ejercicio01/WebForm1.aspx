﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Ejercicio01.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Label ID="LabelNombre" runat="server" Text="Nombre: "></asp:Label>
            <asp:TextBox ID="TextBoxNombre" runat="server"></asp:TextBox>

        </div>
        <p>
            <asp:Label ID="LabelApellido" runat="server" Text="Apellido: "></asp:Label>
            <asp:TextBox ID="TextBoxApellido" runat="server"></asp:TextBox>
        </p>
        <asp:Button ID="ButtonAceptar" runat="server" OnClick="ButtonAceptar_Click" Text="Aceptar" />
        <p>
            <asp:Label ID="LabelResultado" runat="server"></asp:Label>
        </p>
    </form>
</body>
</html>
